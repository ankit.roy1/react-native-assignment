import FlatButton from "./flatButton";
import { 
    resetLaps,
    setMinutesToZero,
    setSecondsToZero,
    setMillisecondToZero

} from "../actions/stopWatchActions";
import { useDispatch,useSelector } from 'react-redux';

function ResetButton(){
    const dispatch=useDispatch()
    const {isRunning} =useSelector(state=>state)

    const handleReset=()=>{
        if(!isRunning){
            dispatch(setMillisecondToZero())
            dispatch(setSecondsToZero())
            dispatch(setMinutesToZero())
            dispatch(resetLaps())
        }
    }
    
    return (
        <FlatButton text='Reset' onPress={handleReset}/>
    )
}

export default ResetButton;