import FlatButton from "./flatButton";
import { start } from './../actions/stopWatchActions';
import { useDispatch,useSelector } from 'react-redux';

function StartButtton(){
    const dispatch=useDispatch();
    
    const handleStart=()=>{
        dispatch(start())
    }
    
    return(
        <FlatButton text='Start' onPress={handleStart}/>
    )
}

export default StartButtton;