import FlatButton from "./flatButton";
import { useDispatch } from 'react-redux';
import { stop } from './../actions/stopWatchActions';

function StopButton(){
    const dispatch=useDispatch()

    const handleStop=()=>{
        dispatch(stop())
    }
    return(
        <FlatButton text='Stop' onPress={handleStop}/>
    )
}

export default StopButton;