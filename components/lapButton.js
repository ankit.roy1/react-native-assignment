import FlatButton from "./flatButton";
import { useSelector,useDispatch } from 'react-redux';
import { lap } from './../actions/stopWatchActions'

function LapButton(){
    
    const dispatch=useDispatch()
    const {minutes,seconds,milliseconds}=useSelector(state=>state)
    
    const createLap=()=>{
        dispatch(lap({minutes,seconds,milliseconds}))
    }

    return(
        <FlatButton text='Lap' onPress={createLap}/>
    )
}

export default LapButton;