import { View,Text,ScrollView,StyleSheet} from 'react-native';
import { useSelector } from 'react-redux';
import { padTime } from './../utils';
function Lap(){
    const { lap }=useSelector(state=>state)
    return(
        <ScrollView style={styles.lapContainer}>
            {
                lap.map((time,index)=>(
                    <View style={styles.lapItem} key={index}>
                        <Text style={styles.lapText}>
                            {`#${padTime(index)} ${padTime(time.minutes)}:${padTime(time.seconds)}:${padTime(time.milliseconds)}`}
                        </Text>
                    </View>
                ))
            }
        </ScrollView>
    )
}

const styles=StyleSheet.create({
    lapContainer:{
        maxHeight:300,
        minWidth:200,
        marginTop:50,
        marginBottom:50,
        // backgroundColor:'#333'
    },
    lapItem:{
        marginVertical:10,
        marginHorizontal:20,
        backgroundColor:'#f1356d',
        padding:15,
        borderRadius:15,
        color:'#f5f5f3'
    },
    lapText:{
        color:'white'
    }

})

export  default Lap;