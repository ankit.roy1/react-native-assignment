import {StyleSheet,View,Text,TouchableOpacity} from 'react-native';


function FlatButton({text,onPress}){
    return(
        <TouchableOpacity onPress={onPress}>
            <View style={styles.button}>
                <Text style={styles.buttonText}>
                    {text}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

const styles=StyleSheet.create({
    button:{
        minWidth:70,
        borderRadius:8,
        paddingVertical:14,
        paddingHorizontal:10,
        backgroundColor:'#f1356d',
        marginLeft:10
    },
    buttonText:{
        color: '#f5f5f3',
        fontWeight:'bold',
        textTransform:'uppercase',
        fontSize:16,
        textAlign:'center'
    }
});

export default FlatButton;