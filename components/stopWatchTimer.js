import { useEffect } from 'react';
import { padTime } from './../utils'
import { StyleSheet,View,Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { 
    setMinutes,
    setSeconds,
    setSecondsToZero,
    stop, 
    setMillisecond, 
    setMillisecondToZero 
} from './../actions/stopWatchActions';

function StopWatchTimer(){

    const dispatch=useDispatch()
    const {minutes,seconds,milliseconds,isRunning}=useSelector(state=>state)

    useEffect(()=>{
        let timerId;
        if(isRunning===true){
          if(minutes===59 && seconds===59){
            dispatch(stop())
          }
          timerId=setInterval(()=>{
            if(milliseconds+1 >= 99){
              dispatch(setSeconds())
              dispatch(setMillisecondToZero())
            }else{
              dispatch(setMillisecond())
            }
    
            if(seconds+1===60){
              dispatch(setMinutes())
              dispatch(setSecondsToZero())
            }
            
          },10)
          return ()=>window.clearInterval(timerId)
        }
    
      },[isRunning,minutes,seconds,milliseconds,dispatch])
    
    return(
        <View style={styles.timer}>
            <Text style={styles.time}>{padTime(minutes)}</Text>
            <Text style={styles.sepator}>:</Text>
            <Text style={styles.time}>{padTime(seconds)}</Text>
            <Text style={styles.sepator}>:</Text>
            <Text style={styles.time}>{padTime(milliseconds)}</Text>
        </View>
    )
}

const styles=StyleSheet.create({
    timer:{
        display: 'flex',
        flexDirection:'row',
        justifyContent:'center'
    },
    time:{
        fontSize:48
    },
    sepator:{
        fontSize:42
    }
})

export default StopWatchTimer;