function padTime(time) {
    return time.toString().padStart(2, "0");
}

export {padTime}