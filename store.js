import {createStore} from 'redux';
import {stopWatchReducer} from "./reducers/stopWatchReducer"

const store=createStore(stopWatchReducer)

export {store}