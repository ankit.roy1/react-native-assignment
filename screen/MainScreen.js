import { StyleSheet, View } from 'react-native';
import StartButtton from '../components/startButton';
import StopButton from '../components/stopButton';
import ResetButton from '../components/resetButton';
import LapButton from '../components/lapButton';
import StopWatchTimer from '../components/stopWatchTimer';
import Lap from '../components/laps';
import { useSelector } from 'react-redux';


export default function Main() {
  const isRunning=useSelector(state=>state.isRunning);
  return (
      <View style={styles.container}>
        <StopWatchTimer />
        <Lap />
        <View style={styles.buttonGroup}>
          {!isRunning && <StartButtton />}
          {isRunning && <StopButton />}
          <LapButton />
          <ResetButton />
        </View>
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonGroup:{
    display:'flex' ,
    flexDirection:'row',
  }
});
